# Deployments

## Build step

Since this site is meant to be a subset of pages built for about.gitlab.com, with the rest of it handled in [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com), we disable the [crawler attribute of the generator](https://nuxtjs.org/docs/2.x/configuration-glossary/configuration-generate#crawler). Missing relative links are assumed to be created elsewhere.

## GitLab CI

Refer to the `.gitlab-ci.yml` file for up to date documentation on the pipeline. Since pipelines can grow in scope and get complex to follow, prefer documenting the `.gitlab-ci.yml` file with inline comments over updating documentation in this markdown file. 

## Deploy script

Deployments are handled through a shell script in `scripts/deploy`. As with the `.gitlab-ci.yml` file, prefer documenting decisions in that file directly rather than in this file. At a high level, the deploy script should be able to be called from the CI pipeline and take contextual deployment actions based on variables made available in the CI environment it has been called from. That way we can manage the CI complexity in one place (`.gitlab-ci.yml`), and manage deployment complexity mostly in one place (`scripts/deploy`).
