---
  title: The One DevOps Platform | GitLab
  description: From planning to production, bring teams together in one application. Ship secure code faster, deploy to any cloud, and drive business results.
  schema_org: >
    {"@context":"https://schema.org","@type": "Corporation","name":"GitLab","legalName":"GitLab Inc.","tickerSymbol":"GTLB","url":"https://about.gitlab.com","logo":"https://gitlab.com/gitlab-com/gitlab-artwork/raw/master/logo/logo.png", "description" : "GitLab is The DevOps platform that empowers organizations to maximize the overall return on software development by delivering software faster and efficiently, while strengthening security and compliance. With GitLab, every team in your organization can collaboratively plan, build, secure, and deploy software to drive business outcomes faster with complete transparency, consistency and traceability.GitLab is an open core company which develops software for the software development lifecycle with 30 million estimated registered users and more than 1 million active license users, and has an active community of more than 2,500 contributors. GitLab openly shares more information than most companies and is public by default, meaning our projects, strategy, direction and metrics are discussed openly and can be found within our website. Our values are Collaboration, Results, Efficiency, Diversity, Inclusion & Belonging , Iteration, and Transparency (CREDIT) and these form our culture.","foundingDate":"2011","founders":[{"@type":"Person","name":"Sid Sijbrandij"},{"@type":"Person","name":"Dmitriy Zaporozhets"}],"slogan": "Our mission is to change all creative work from read-only to read-write so that everyone can contribute.","address":{"@type":"PostalAddress","streetAddress":"268 Bush Street #350","addressLocality":"San Francisco","addressRegion":"CA","postalCode":"94104","addressCountry":"USA"},"awards": "Comparably's Best Engineering Team 2021, 2021 Gartner Magic Quadrant for Application Security Testing - Challenger, DevOps Dozen award for the Best DevOps Solution Provider for 2019, 451 Firestarter Award from 451 Research","knowsAbout": [{"@type": "Thing","name": "DevOps"},{"@type": "Thing","name": "CI/CD"},{"@type": "Thing","name": "DevSecOps"},{"@type": "Thing","name": "GitOps"},{"@type": "Thing","name": "DevOps Platform"}],"sameAs":["https://www.facebook.com/gitlab","https://twitter.com/gitlab","https://www.linkedin.com/company/gitlab-com","https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg"]}
  cta_block:
    title: "The One DevOps Platform"
    subtitle: "From planning to production, bring teams together in one application. Ship secure code faster, deploy to any cloud, and drive business results."
    badge:
      text: See what's coming in GitLab 15 and beyond →
      link: "https://page.gitlab.com/fifteen.html"
      data_ga_name: See what's coming in GitLab 15 and beyond
      data_ga_location: hero
    primary_button:
      text: "Get free trial"
      link: "https://gitlab.com/-/trials/new?glm_content=default-saas-trial&glm_source=about.gitlab.com"
      data_ga_name: "free trial"
      data_ga_location: "hero"
    secondary_button:
      text: "What is GitLab?"
      modal:
        video_link: https://player.vimeo.com/video/702922416?h=06212a6d7c
      data_ga_name: "watch demo"
      data_ga_location: "hero"
    images:
      - id: 1
        image: "/nuxt-images/home/Verify.png"
        alt: "Brand image of GitLab product"
      - id: 2
        image: "/nuxt-images/home/Manage.png"
        alt: "Brand image of GitLab boards"
      - id: 3
        image: "/nuxt-images/home/Plan.png"
        alt: "Brand image of GitLab roadmap"
  customer_logos_block:
    showcased_enterprises:
      - image_url: "/nuxt-images/home/logo_tmobile_mono.svg"
        link_label: Link to T-Mobile and GitLab webcast landing page"
        alt: "T-Mobile logo"
        url: /webcast/aws-tmobile-gitlab/
      - image_url: "/nuxt-images/home/logo_goldman_sachs_mono.svg"
        link_label: Link to Goldman Sachs customer case study
        alt: "Goldman Sachs logo"
        url: "/customers/goldman-sachs/"
      - image_url: "/nuxt-images/home/logo_cncf_mono.svg"
        link_label: Link to Cloud Native Computing Foundation customer case study
        alt: "Cloud Native logo"
        url: /customers/cncf/
      - image_url: "/nuxt-images/home/logo_siemens_mono.svg"
        link_label: Link to Siemens customer case study
        alt: "Siemens logo"
        url: /customers/siemens/
      - image_url: "/nuxt-images/home/logo_nvidia_mono.svg"
        link_label: Link to Nvidia customer case study
        alt: "Nvidia logo"
        url: /customers/Nvidia/
      - image_url: "/nuxt-images/home/logo_ubs_mono.svg"
        link_label: Link to blogpost How UBS created their own DevOps platform using GitLab
        alt: "UBS logo"
        url: /blog/2021/08/04/ubs-gitlab-devops-platform/
  resource_card_block:
    column_size: 4
    header_cta_text: View all resources
    header_cta_href: /resources/
    header_cta_ga_name: view all resources
    header_cta_ga_location: body
    cards:
      - icon:
          name: ebook-alt
          variant: marketing
          alt: Ebook Icon
        event_type: "Ebook"
        header: "Beginner's Guide to DevOps"
        link_text: "Read more"
        href: "https://page.gitlab.com/resources-ebook-beginners-guide-devops.html"
        image: "/nuxt-images/home/resources/Devops.png"
        alt: Winding path
        data_ga_name: "Beginner's Guide to DevOps"
        data_ga_location: "body"
      - icon:
          name: topics
          variant: marketing
          alt: Topics Icon
        event_type: "Topics"
        header: "What is CI/CD?"
        link_text: "Read more"
        href: "/topics/ci-cd/"
        image: "/nuxt-images/resources/fallback/img-fallback-cards-cicd.png"
        alt: Text reading CI/CD on a gradient
        data_ga_name: "Collaboration without Boundaries"
        data_ga_location: "body"
      - icon:
          name: report-alt
          variant: marketing
          alt: Report Icon
        event_type: "Report"
        header: "2021 Global DevSecOps Survey"
        link_text: "Read more"
        href: "/developer-survey/"
        image: "/nuxt-images/home/resources/Survey.png"
        alt: Checkmark in sidewalk chalk
        data_ga_name: "2021 Global DevSecOps Survey"
        data_ga_location: "body"
      - icon:
          name: blog-alt
          variant: marketing
          alt: Blog Icon
        event_type: "Blog post"
        header: "Comply with NIST's secure software supply chain framework with GitLab"
        link_text: "Read more"
        href: "/blog/2022/03/29/comply-with-nist-secure-supply-chain-framework-with-gitlab/"
        image: "/nuxt-images/home/resources/NIST.png"
        alt: City at night
        data_ga_name: "Comply with NIST's secure software supply chain framework with GitLab"
        data_ga_location: "body"
      - icon:
          name: partners
          variant: marketing
          alt: Partners Icon
        event_type: "Partners"
        header: "Discover the benefits of GitLab on AWS"
        link_text: "Read more"
        href: "/partners/technology-partners/aws/"
        image: "/nuxt-images/home/resources/AWS.png"
        alt: "Text reading AWS on a gradient"
        data_ga_name: "Discover the benefits of GitLab on AWS"
        data_ga_location: "body"
      - icon:
          name: announce-release
          variant: marketing
          alt: Announce Release Icon
          hex_color: '#FFF'
        event_type: "Release"
        header: "GitLab 15.1 released with SAML Group Sync and SLSA level 2 build artifact attestation"
        link_text: "Read more"
        href: "/releases/2022/06/22/gitlab-15-1-released/"
        alt: "GitLab version number on a gradient"
        image: "/nuxt-images/home/resources/15_1.png"
        data_ga_name: "GitLab 15.1 released with SAML Group Sync and SLSA level 2 build artifact attestation"
        data_ga_location: "body"

  quotes_carousel_block:
    header: Teams do more with GitLab
    header_link:
      url: /customers/
      text: Read our case studies
      data_ga_name: Read our case studies
      data_ga_location: body
    quotes:
      - title_img:
          url: /nuxt-images/home/logo_ubs_mono.svg
          alt: UBS logo
        main_img:
          url: /nuxt-images/home/quotes/ubs.png
          alt: Headshot of Rick Carey UBS employee
        quote: "\u0022We have an expression at UBS, ‘all developers wait at the same speed,’ so anything we can do to reduce their waiting time is value added. And GitLab allows us to have that integrated experience. Our developers should be able to build cloud-native applications on a cloud-native platform and have the best-in-class developer experience. That was our goal.\u0022"
        author: Rick Carey Group Chief Technology Officer
        url: /blog/2021/08/04/ubs-gitlab-devops-platform/
      - title_img:
          url: /nuxt-images/home/logo_knowbe4_mono.svg
          alt: KnowBe4 logo
        main_img:
          url: /nuxt-images/home/quotes/knowbe4.png
          alt: Headshot of KnowBe4 employee
        quote: "\u0022There's literally no other solution that does everything that GitLab does.\u0022"
        author: Alex Callihan, VP of Platform Engineering, KnowBe4
        url: /customers/knowbe4/
      - title_img:
          url: /nuxt-images/home/logo_iron_mountain_mono.svg
          alt: Iron Mountain logo
        main_img:
          url: /nuxt-images/home/quotes/iron_mountain.png
          alt: Headshot of Iron Mountain employee
        quote: "\u0022What's great about GitLab is the single pane of glass it provides for scaling an Agile model.\u0022"
        author: Josh Langley, VP of Enterprise Architecture and Platforms, Iron Mountain
      - title_img:
          url: /nuxt-images/home/logo_siemens_mono.svg
          alt: Siemens logo
        main_img:
          url: /nuxt-images/home/quotes/siemens.png
          alt: Headshot of Siemens employee
        quote: "\u0022Our customers and developers want to have a reliable service [like Gitlab] that is running all the time.\u0022"
        author: Roger Meier, Principal Key Expert and Service Owner of code.siemens.com from Siemens IT
        url: /customers/siemens/

  devops_lifecycle:
    title: "For every stage of the DevOps Lifecycle"
    tagline: "Eliminate point solution tool sprawl with our comprehensive platform."
    stages:
      - title: "Plan"
        ga_carousel_value: plan
        icon:
          name: plan
          alt: Plan Icon
          variant: marketing
        tagline: "Keep everyone synchronized with powerful planning tools, regardless of your process."
        description: "Whether your methodology is Waterfall or DevOps, GitLab’s simple and flexible approach to planning helps you stay organized and track progress. Now you can ensure teams are working on the right things at the right time, and maintain end-to-end visibility and traceability of issues throughout the delivery lifecycle – from idea to production."
        link:
          text: Learn more
          url: /stages-devops-lifecycle/plan/
          ga_name: plan learn more
        image: /nuxt-images/home/devops-lifecycle/plan.png
        alt: "Brand image of Plan"
      - title: "Create"
        ga_carousel_value: create
        icon:
          name: create
          alt: Create Icon
          variant: marketing
        tagline: "Create, view, and manage code and project data through powerful branching tools."
        description: "GitLab helps teams design, develop, and securely manage code and project data from a single distributed version control system to enable rapid iteration. GitLab repositories provide a scalable, single source of truth for collaborating on projects and code so teams can be productive without disrupting their workflows."
        link:
          text: Learn more
          url: /stages-devops-lifecycle/create/
          ga_name: create learn more
        image: /nuxt-images/home/devops-lifecycle/create.png
        alt: "Brand image of Create"
      - title: "Verify"
        ga_carousel_value: verify
        icon:
          name: verify
          alt: Verify Icon
          variant: marketing
        tagline: Automatically verify your code with powerful <a href="https://about.gitlab.com/features/continuous-integration/">continuous integration (CI)</a> capabilities.
        description: "GitLab helps you automate the builds, integration, and verification of your code. With SAST, DAST, code quality analysis, plus pipelines that enable concurrent testing and parallel execution, your teams get quick insights about every commit so they can deliver higher quality code faster."
        link:
          text: Learn more
          url: /stages-devops-lifecycle/verify/
          ga_name: verify learn more
        image: /nuxt-images/home/devops-lifecycle/verify.png
        alt: "Brand image of Verify"
      - title: "Package"
        ga_carousel_value: package
        icon:
          name: package
          alt: Package Icon
          variant: marketing
        tagline: "Create a dependable software supply chain with built-in package management."
        description: "GitLab enables teams to package their applications and dependencies, manage containers, and build artifacts with ease. The private, secure, container and package registry are built-in and preconfigured out-of-the- box to work seamlessly with GitLab source code management and CI/CD pipelines."
        link:
          text: Learn more
          url: /stages-devops-lifecycle/package/
          ga_name: package learn more
        image: /nuxt-images/home/devops-lifecycle/package.png
        alt: "Brand image of Package"
      - title: "Secure"
        ga_carousel_value: secure
        icon:
          name: secure
          alt: Secure Icon
          variant: marketing
        tagline: "Deliver secure applications with security testing and scanning built in."
        description: "GitLab provides Static Application Security Testing (SAST), Dynamic Application Security Testing (DAST), Container Scanning, and Dependency Scanning to help you deliver secure applications along with license compliance."
        link:
          text: Learn more
          url: /stages-devops-lifecycle/secure/
          ga_name: secure learn more
        image: /nuxt-images/home/devops-lifecycle/secure.png
        alt: "Brand image of Secure"
      - title: "Release"
        ga_carousel_value: release
        icon:
          name: release
          alt: Release Icon
          variant: marketing
        tagline: "Ship code with zero-touch continuous delivery (CD) built into the pipeline."
        description: "With GitLab, deployments can be automated to multiple environments like staging and production. Even for more advanced patterns like canary deployments, the system just knows what to do without being told. You also have feature flags, built-in auditing and traceability, on-demand environments, and GitLab pages for static content delivery, so you can deliver faster and with more confidence than ever before."
        link:
          text: Learn more
          url: /stages-devops-lifecycle/release/
          ga_name: release learn more
        image: /nuxt-images/home/devops-lifecycle/release.png
        alt: "Brand image of Release"
      - title: "Configure"
        ga_carousel_value: configure
        icon:
          name: configure
          alt: Configure Icon
          variant: marketing
        tagline: "Configure your applications and infrastructure simply and securely."
        description: "GitLab helps teams configure and manage their application environments. Strong integration with Kubernetes reduces the effort needed to define and configure the infrastructure required to support your application. Protect access to key infrastructure configuration details such as passwords and login information by using ‘secret variables’ to limit access to only authorized users and processes."
        link:
          text: Learn more
          url: /stages-devops-lifecycle/configure/
          ga_name: configure learn more
        image: /nuxt-images/home/devops-lifecycle/configure.png
        alt: "Brand image of Configure"
      - title: "Monitor"
        ga_carousel_value: monitor
        icon:
          name: monitor
          alt: Monitor Icon
          variant: marketing
        tagline: "Help reduce the severity and frequency of incidents."
        description: "Get the feedback and tools you need to reduce the severity and frequency of incidents — and release software more frequently and confidently. From error tracking to incident management, GitLab has you covered."
        link:
          text: Learn more
          url: /stages-devops-lifecycle/monitor/
          ga_name: monitor learn more
        image: /nuxt-images/home/devops-lifecycle/monitor.png
        alt: "Brand image of Monitor"
      - title: "Protect"
        ga_carousel_value: protect
        icon:
          name: protect
          alt: Protect Icon
          variant: marketing
        tagline: Prevent security intrusions with <a href="https://docs.gitlab.com/ee/user/application_security/container_scanning/">container scanning</a> and cloud-native protections.
        description: "GitLab provides cloud-native protections — including unified policy management, container scanning, and container network and host security — to help you protect your apps and infrastructure from security intrusions."
        link:
          text: Learn more
          url: /stages-devops-lifecycle/protect/
          ga_name: protect learn more
        image: /nuxt-images/home/devops-lifecycle/protect.png
        alt: "Brand image of Protect"
      - title: "Manage"
        ga_carousel_value: manage
        icon:
          name: manage
          alt: Manage Icon
          variant: marketing
        tagline: Get the metrics and <a href="https://about.gitlab.com/solutions/value-stream-management/">value stream</a> insights to optimize your software delivery lifecycle.
        description: "With GitLab, you gain the visibility and insights you need to understand performance, manage and optimize your software delivery lifecycle, and increase your delivery velocity."
        link:
          text: Learn more
          url: /stages-devops-lifecycle/manage/
          ga_name: manage learn more
        image: /nuxt-images/home/devops-lifecycle/manage.png
        alt: "Brand image of Manage"

  solutions_block:
    title: One platform, one team
    image: /nuxt-images/home/solutions/solutions-top-down.png
    alt: "Top down image of office"
    description: "Whether you're starting by integrating a few point solutions, or simplifying your entire toolchain, now you can do it as one team in one platform. Collaborating from planning to production across one platform, with security built-in."
    subtitle: The way DevOps should be
    sub_image: /nuxt-images/home/solutions/solutions.png
    solutions:
      - title: Accelerate your digital transformation
        description: Reach your digital transformation objectives faster with a DevOps platform for your entire organization.
        icon:
          name: accelerate
          alt: Accelerate Icon
          variant: marketing
        link_text: Learn More
        link_url: /solutions/digital-transformation/
        data_ga_name: digital transformation
        data_ga_location: body
        image: /nuxt-images/home/solutions/accelerate.png
        alt: "Text bubbles of communicating teams"
      - title: Deliver software faster
        description: Automate your software delivery process so you can deliver value faster and quality code more often.
        icon:
          name: deliver
          alt: Deliver Icon
          variant: marketing
        link_text: Learn More
        link_url: /solutions/delivery-automation/
        data_ga_name: delivery automation
        data_ga_location: body
        image: /nuxt-images/home/solutions/deliver.png
        alt: "Text bubbles of communicating teams"
      - title: Ensure compliance
        description: Simplify continuous software compliance by defining, enforcing and reporting on compliance in one platform.
        icon:
          name: ensure
          alt: Ensure Icon
          variant: marketing
        link_text: Learn More
        link_url: /solutions/continuous-software-compliance/
        data_ga_name: continuous software compliance
        data_ga_location: body
        image: /nuxt-images/home/solutions/ensure.png
        alt: "Text bubbles of communicating teams"
      - title: Build in security
        description: Adopt DevSecOps practices with continuous software security assurance across every stage.
        icon:
          name: shield-check-light
          alt: Shield Check Icon
          variant: marketing
        link_text: Learn More
        link_url: /solutions/continuous-software-security-assurance/
        data_ga_name: continuous software security assurance
        data_ga_location: body
        image: /nuxt-images/home/solutions/build.png
        alt: "Text bubbles of communicating teams"
      - title: Improve collaboration and visibility
        description: Give everyone one platform to collaborate and see everything from planning to production.
        icon:
          name: improve
          alt: Improve Icon
          variant: marketing
        link_text: Learn More
        link_url: /solutions/devops-platform/
        data_ga_name: devops platform
        data_ga_location: body
        image: /nuxt-images/home/solutions/improve.png
        alt: "Text bubbles of communicating teams"

